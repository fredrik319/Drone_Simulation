﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO.Ports;
using System.Threading;

public class FingerCollision : MonoBehaviour
{
    //This script handles the Arduino transitions and distance calculations
    [SerializeField]
    float range;
    public Text rangeText;

    private Transform t;
    private Transform obsticle;

    //Arduino properties
    public static SerialPort sp = new SerialPort("COM3", 9600);

    // at initiation, we set the position of drone and box
    private void Awake()
    {
        t = this.transform;
        obsticle = GameObject.FindGameObjectWithTag("collision").transform;
    }

    // Use this for initialization
    void Start ()
    {
        //Arduino connection established
        OpenConnection();
        range = 0;
        setRangeText();
    }

    // Update is called once per frame
     void Update ()
    {
        //Sets the range between box and drone and constantly updates it
        Distance();
        setRangeText();

    }

    void setRangeText()
    {
        rangeText.text = "Distance to box: "+ range.ToString();
    }

    // This takes the vector distance between the position of drone and box and returns the value of the range between
    public float Distance()
    {
        range = Vector3.Distance(t.position, obsticle.position);
        return range;
    }

    //Three functions for writing a string to the Arduino IDE, 2 is middle rumbling, 3 is hard rumbling, 0 is no rumbling
    public static void send2()
    {
        sp.Write("2");
    }
    public static void send3()
    {
        sp.Write("3");
    }
    public static void send0()
    {
        sp.Write("0");
        
    }

    //An attempt to send according to range, did not work well enough
   /* public void sendLevel()
    {
        if (range <= 20.0f)
        {
            sp.Write("2");
        }
        else if (range >= 20 && range <= 50)
        {
            sp.Write("3");
        }
        else
        {
            sp.Write("0");
        }

    }*/

    //Close the port to arduino IDE
    public void Close()
    {
        sp.Close();
    }
    //Function for handling the connection to the arduino IDE
    public void OpenConnection()
    {
        if (sp != null)
        {
            if (sp.IsOpen)
            {
                sp.Close();
                print("Closing port, because it was already open!");
            }
            else
            {
                sp.Open();  // opens the connection
                //sp.ReadTimeout = 16;  // sets the timeout value before reporting error
                print("Port Opened!");
                //		message = "Port Opened!";
            }
        }
        else
        {
            if (sp.IsOpen)
            {
                print("Port is already open");
            }
            else
            {
                print("Port == null");
            }
        }
    }
}
