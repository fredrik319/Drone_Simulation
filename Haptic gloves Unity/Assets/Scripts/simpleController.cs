﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;

public class simpleController : MonoBehaviour
{
    //This is the script we had to go for to control the drone with Leap Motion
    public float speed;

    private Rigidbody rb;

    //Hand controllers
    Controller m_leapController;
    Hand hand;
    List<Hand> hands;
    Frame frame;

    //Values of hand movements
    float HandPalmPitch;
    float HandPalmYaw;
    float HandPalmRoll;
    float HandWristRot;

    //Directions to move
    public float up;
    float down;
    float right;
    float left;
    public float forward;
    float backward;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //Initiate hand controller
        m_leapController = new Controller();
        frame = m_leapController.Frame();
        hands = frame.Hands;

        if (frame.Hands.Count > 0)
        {
            Hand firstHand = hands[0];
        }

        //Sets pitch, roll and yaw to the hands equivalent pitch, roll and yaw
        HandPalmPitch = hands[0].PalmNormal.Pitch;
        HandPalmRoll = hands[0].PalmNormal.Roll;
        HandPalmYaw = hands[0].PalmNormal.Yaw;
        HandWristRot = hands[0].WristPosition.Pitch;

        //Tell the console which values we get from the hand
        Debug.Log("Pitch :" + HandPalmPitch);
        Debug.Log("Roll :" + HandPalmRoll);
        Debug.Log("Yaw :" + HandPalmYaw);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //To move either horizontal or vertical
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Tells the drone when to move forward og backwards
        if (-2f < HandPalmYaw && HandPalmYaw < 0f)
        {
            forward += 1f;
        }
        else if (0f < HandPalmYaw && HandPalmYaw < 2f)
        {
            forward += -1f;
        }
        else
        {
            forward = 0f;
        }

        //Tells the drone when to move up or down
        if (-2f < HandPalmPitch && HandPalmPitch < 0f)
        {
            up += 1f;
        }
        else if (0f < HandPalmPitch && HandPalmPitch < 2f)
        {
            up += -1f;
        }
        else
        {
            up = 0f;
        }

        //Set the limit of force to the drone
        up = Mathf.Clamp(up, 0f, 30f);
        forward = Mathf.Clamp(forward, -30f, 30f);

        //Get the vectors for the movement
        Vector3 movement = new Vector3(forward, up, 0f);

        //Add the force to the rigidbody
        rb.AddForce(movement * speed);

    }
}

