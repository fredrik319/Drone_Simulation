﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;

public class moveTest : MonoBehaviour {
    Controller control;
    float HandPalmPitch;
    float HandPalmYaw;
    float HandPalmRoll;
    float HandWristRot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        control = new Controller();
        Frame frame = control.Frame();
        List<Hand> hands = frame.Hands;
        if(frame.Hands.Count > 0)
        {
            Hand firstHand = hands[0];
        }

        HandPalmPitch = hands[0].PalmNormal.Pitch;
        HandPalmRoll = hands[0].PalmNormal.Roll;
        HandPalmYaw = hands[0].PalmNormal.Yaw;

        HandWristRot = hands[0].WristPosition.Pitch;

        if(HandPalmYaw > -2f && HandPalmYaw < 3.5f)
        {
            transform.Translate(new Vector3(1, 0, 0 * Time.deltaTime));
        }else if (HandPalmYaw < -2.2f)
        {
            transform.Translate(new Vector3(-1, 0, 0 * Time.deltaTime));
        }
    }
}
