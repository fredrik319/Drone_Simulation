﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxScript : MonoBehaviour {
    // Is true when drone is within collision radius
    bool stay;
	

	// Update is called once per frame
	void Update () {

        //if drone is within collision radius, send fast rumble to arduino IDE
		if(stay == true)
        {
            FingerCollision.send3();
        }

        //If not send no rumble to arduino IDE
        else
        {
            FingerCollision.send0();
        }
	}
    // if the drone enter the box collider, the trigger will be triggered and the bool sets to true, if the drone exits, the bool is false
    public void OnTriggerEnter(Collider colli)
    {
        if (colli.tag == "Player" && stay == false)
        {
            
            stay = true;
        }
        
    }
    public void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" && stay == true)
        {
            stay = false;
        }
    }
}
