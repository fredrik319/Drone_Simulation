﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    //Script for making the camera follow the drone
    public GameObject player;

    private Vector3 offset;

	// Use this for initialization
	void Start () {
        //Set the difference between the position of camera and drone
        offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        //Set that camera position is the same according to the drone at any time
        transform.position = player.transform.position + offset;
	}
}
