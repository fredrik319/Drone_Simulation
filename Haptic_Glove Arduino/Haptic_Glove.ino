#include <SD.h>
#include <SoftwareSerial.h>
#include <SerialCommand.h>
SerialCommand sCmd;

const int pin = 30; //connected to pin 30 on Arduino

void setup()
{
	Serial.begin(9600);
	while (!Serial);
	sCmd.addCommand("PING", pingHandler);
	sCmd.addCommand("ECHO", echoHandler);
	pinMode(pin, OUTPUT);
}

void loop()
{
	if (Serial.available() > 0)
		sCmd.readSerial();

	digitalWrite(pin, HIGH);
	delay(5000);
	digitalWrite(pin, LOW);
	delay(5000);
}

void pingHandler()
{
  Serial.println("PONG");
}

void echoHandler()
{
	char *arg;
	arg = sCmd.next();
	if (arg != NULL)
		Serial.println(arg);
	else
		Serial.println("nothing to echo");
}
